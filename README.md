# Configuring the application:
* Clone files into your server
* Configure your database connection in the .ENV file
* Run database migrations. ('php artisan migrate')
* Create new user through console ('php artisan user:create')
* You can now login and start using the app.

# Testing the application:
* You can run tests by running 'phpunit' command in your terminal