<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RegistrationTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {

        $this->visit('/blog/public/feeds/create')
            ->type('Taylor', 'title')
            ->type('testcase@email.com', 'url')
            ->seePageIs('/blog/public/feeds/create');
    }
}
