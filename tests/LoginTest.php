<?php

class LoginTest extends TestCase
{
    /**
     * Testing logging in when trying to access a protected page
     *
     */
    public function testExample()
    {

        $user = factory(App\User::class)->create([
            'email' => 'test18@testxample.com',
            'password' => bcrypt('testpass123')
        ]);

        $this->visit('/feeds')
            ->type($user->email, 'email')
            ->type('testpass123', 'password')
            ->press('Login')
            ->see('Feeds manager')
            ->seePageIs('/feeds');
    }
}
