<?php

use App\User;

class FeedTest extends TestCase
{
    /**
     *  Testing new feed creation
     */
    public function testCreate()
    {

        $user = new User(array('name' => 'John'));
        $this->be($user);

        $this->visit('/feeds/create')
            ->type('Test title', 'title')
            ->type('testcase@email.com', 'url')
            ->press('Add new feed')
            ->seePageIs('/feeds/create');
    }

    /**
     *  Testing new feed creation with invalid URL
     */
    public function testCreateInvalidURL()
    {

        $user = new User(array('name' => 'John'));
        $this->be($user);

        $this->visit('/feeds/create')
            ->type('Test title', 'title')
            ->type('justastring', 'url')
            ->press('Add new feed')
            ->see('The url format is invalid.')
            ->seePageIs('/feeds/create');
    }

    /**
     *  Testing feed creation form validation
     */
    public function testValidation()
    {

        $user = new User(array('name' => 'John'));
        $this->be($user);

        $this->visit('/feeds/create')
            ->type('', 'title')
            ->type('', 'url')
            ->press('Add new feed')
            ->see('The title field is required.')
            ->see('The url field is required.')
            ->seePageIs('/feeds/create');
    }
}
