<?php

use App\User;

class CategoryTest extends TestCase
{
    /**
     * Testing accessing a protected categories page
     */
    public function testIndex()
    {
        $user = new User(array('name' => 'John'));
        $this->be($user);

        $this->visit('/categories')
            ->see('Categories')->seePageIs('/categories');
    }

    /**
     * Trying to filter by a non-existent category
     */
    public function testNonExistent()
    {

        $this->visit('/filter/randomstuff55')
            ->see('No feeds found for this category.')
            ->seePageIs('/filter/randomstuff55');
    }
}
