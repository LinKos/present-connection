<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('login', function () {
    return view('welcome');
});


Auth::routes();
Route::get('/', 'HomeController@index');
Route::get('/filter/{cat?}', 'HomeController@index');

Route::resource('feeds', 'FeedController');
Route::resource('categories', 'CategoryController');

Route::get('/password', 'UserController@index');
Route::post('/password', 'UserController@updatePassword');