<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = [
        'title', 'url',
    ];

    public function categories()
    {
        return $this->belongsToMany('App\Category');
    }
}
