<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create admin user';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $email = $this->ask('Enter your email:');
        $password = $this->ask('Enter your password:');


        User::create([
            'email' => $email,
            'password' => bcrypt($password),
        ]);

        $this->info('User created. You can now login.');
        return 0;
    }
}
