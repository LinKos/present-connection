<?php
/**
 * Created by PhpStorm.
 * User: Linas
 * Date: 04/01/2017
 * Time: 20:01
 */

namespace App;


class FeedRow
{

    var $title;
    var $link;
    var $description;
    var $pubDate;

    public function __construct($title, $link, $description, $provider, $pubDate)
    {
        $this->title = $title;
        $this->link = $link;
        $this->description = $description;
        $this->provider = $provider;
        $this->pubDate = $pubDate;
    }
    public function doSomething()
    {
        //
    }
}