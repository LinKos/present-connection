<?php

namespace App\Http\Controllers;

use App\Feed;
use App\FeedRow;
use App\Category;

class HomeController extends Controller
{

    public function index($cat = null)
    {

        $categories = Category::pluck('name', 'id')->prepend('Select a category', '');
        $currentCat = null;

        if($cat !== null) {
            $feeds = Feed::whereHas('categories', function ($query) use ($cat) {
                $query->where('category_id', $cat);
            })->get();

            $currentCat = Category::where('id', '=', $cat)->first(['name']);
        }
        else {
            $feeds = Feed::all();
        }

        $rows = collect();

        foreach ($feeds as $feed) {
            $url = $feed->url;

            if(@simplexml_load_file($url)) {
                $xml = simplexml_load_file($url);


                $length = count($xml->channel->item);

                for ($i = 0; $i < $length; $i++) {

                    $row = new FeedRow(
                        $xml->channel->item[$i]->title,
                        $xml->channel->item[$i]->link,
                        strip_tags($xml->channel->item[$i]->description, "<img></img>"),
                        $xml->channel->link,
                        strftime("%Y-%m-%d %H:%M:%S", strtotime($xml->channel->item[$i]->pubDate))
                    );

                    $rows->push($row);
                }
            }

        }

        $rows = $rows->sortByDesc('pubDate');
        return view('home', ['newsItems' => $rows, 'categories' => $categories, 'currentCat' => $currentCat]);
    }
}
