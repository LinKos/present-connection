<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use Illuminate\Support\Facades\Hash;
use Session;

class UserController extends Controller
{
    //
    public function index()
    {
        return view('user.password');
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/password')
                ->withErrors($validator)
                ->withInput();
        }


        $user = Auth::user();
        if (Hash::check($request->old_password, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
            Session::flash('flash_message', 'Password updated!');
            return redirect()->back();

        } else {
            Session::flash('flash_error', 'The \'Old password\' field value does not match your current password. ');
            return redirect()->back();
        }
    }
}
