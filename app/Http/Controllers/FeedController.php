<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Feed;
use Illuminate\Support\Facades\Session;

class FeedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $feeds = Feed::all();
        return view('feeds.index')->withFeeds($feeds);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');

        return view('feeds.create', ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|unique:feeds',
            'url' => 'required|url|unique:feeds'
        ]);

        $input = $request->all();

        $feed = new Feed;
        $feed->title = $request->title;
        $feed->url = $request->url;
        $feed->save();

        if($request->get('categories') !== null)
        $feed->categories()->sync($request->get('categories'));

        Session::flash('flash_message', 'New feed URL added!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $feed = Feed::findOrFail($id);

        return view('feeds.show')->withFeed($feed);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = Category::pluck('name', 'id');
        $feed = Feed::find($id);



        return view('feeds.edit', ['categories' => $categories, 'feed' => $feed]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $feed = Feed::findOrFail($id);

        $this->validate($request, [
            'title' => 'required',
            'url' => 'required|url'
        ]);

        $input = $request->all();

        $feed->fill($input)->save();

        $feed->categories()->sync($request->get('categories'));

        Session::flash('flash_message', 'Feed successfully edited!');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
