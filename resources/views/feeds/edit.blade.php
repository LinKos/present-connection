@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Edit RSS feed</h1>
                <hr>
                {!! Form::open([
                    'method' => 'PATCH',
                    'route' => ['feeds.update', $feed->id]
                ]) !!}

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::label('title', 'Feed title:', ['class' => 'control-label']) !!}
                    {!! Form::text('title', $feed->title, ['class' => 'form-control']) !!}
                </div>

                {!! Form::label('Categories') !!}<br />
                {!! Form::select('categories[]', $categories,
                    $feed->categories->pluck('id')->toArray(),
                    ['class' => 'form-control',
                    'multiple' => 'multiple']) !!}


                <div class="form-group">
                    {!! Form::label('url', 'Feed URL:', ['class' => 'control-label']) !!}
                    {!! Form::text('url', $feed->url, ['class' => 'form-control']) !!}
                </div>

                {!! Form::submit('Edit feed', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}

            </div>
        </div>
    </div>
@stop