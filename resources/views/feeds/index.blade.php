@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>RSS Feeds</h1><a href="{{ route('feeds.create') }}" class="btn btn-success">Add feed</a>
                <hr>
                @foreach($feeds as $feed)
                    <h3>{{ $feed->title }}</h3>
                    <p>{{ $feed->url}}</p>
                    <p>
                        <a href="{{ route('feeds.edit', $feed->id) }}" class="btn btn-primary">Edit Feed</a>
                    </p>
                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@stop