@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Add a new RSS feed</h1>
                <hr>
                {!! Form::open([
                    'route' => 'feeds.store'
                ]) !!}

                @if($errors->any())
                    <div class="alert alert-danger">
                        @foreach($errors->all() as $error)
                            <p>{{ $error }}</p>
                        @endforeach
                    </div>
                @endif

                @if(Session::has('flash_message'))
                    <div class="alert alert-success">
                        {{ Session::get('flash_message') }}
                    </div>
                @endif

                <div class="form-group">
                    {!! Form::label('title', 'Feed title:', ['class' => 'control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>

                {!! Form::select('categories[]',
   $categories,
   null,
   ['class' => 'form-control',
   'multiple' => 'multiple']) !!}

                <div class="form-group">
                    {!! Form::label('url', 'Feed URL:', ['class' => 'control-label']) !!}
                    {!! Form::text('url', null, ['class' => 'form-control']) !!}
                </div>

                {!! Form::submit('Add new feed', ['class' => 'btn btn-primary']) !!}

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop