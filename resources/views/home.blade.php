@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            {!! Form::select('category',
              $categories,
              null,
              ['class' => 'cat-select form-control'])
              !!}

            @if($currentCat !== null)
            <h3 class="text-center">Current category: {{ $currentCat->name }}</h3>
            <hr/>
            @else
                <br />
            @endif


            @if(count($newsItems) < 1)
                <h3 class="text-center">No feeds found for this category.</h3>
            @else
            <ul>

            @foreach($newsItems as $item)
                <li><a target="_blank" href="{{ $item->provider }}"><img class="provider-icon" src="{!! asset('images/external.png') !!}" title="Go to provider page"></a><a class="modal-link" data-title="{{ $item->title }}" data-description="{{ $item->description }}" data-link="{{ $item->link }}" data-target=".www" data-toggle="modal">{{ $item->title }}</a> {{ $item->pubDate }}</li>
            @endforeach
            </ul>
                @endif
        </div>
    </div>
</div>

<div class="modal fade www">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p id="description"></p>
            </div>

            <div class="modal-footer">
                <div class="pull-left"> Read full article: <a class="article-link" target="_blank" href="">External link</a></div>
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


@endsection
@section('footer')
    @parent

    <script>
        $(document).on("click", ".modal-link", function () {
            var title = $(this).data('title');
            var description = $(this).data('description');
            var link = $(this).data('link');

            console.log(link);


            $(".modal-header .modal-title").html(title);
            $(".modal-body #description").html(description);
            $("a.article-link").attr('href', link);
        });


        $(function(){
            // bind change event to select
            $('.cat-select').on('change', function () {

                var url = 'filter/' + $(this).val(); // get selected value
                if(window.location.href.indexOf("filter/") > -1) {
                    var url = $(this).val();
                }

                if (url) { // require a URL
                    window.location = url; // redirect
                }
                return false;
            });
        });

    </script>
@endsection
