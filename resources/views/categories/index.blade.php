@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Categories</h1><a href="{{ route('categories.create') }}" class="btn btn-success">Add category</a>
                <hr>
                @foreach($categories as $cat)
                    <h5>{{ $cat->name }}</h5>

                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@stop