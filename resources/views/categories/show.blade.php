@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Add a new RSS feed</h1>
                <hr>
                <h1>{{ $feed->title }}</h1>
                <p class="lead">{{ $feed->description }}</p>
                <hr>
                <div class="pull-right">
                    <a href="#" class="btn btn-danger">Delete this feed</a>
                </div>
            </div>
        </div>
    </div>
@stop